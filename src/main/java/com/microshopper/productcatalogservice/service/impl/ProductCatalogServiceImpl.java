package com.microshopper.productcatalogservice.service.impl;

import com.microshopper.manufacturerservice.web.api.ManufacturerApi;
import com.microshopper.manufacturerservice.web.dto.Manufacturer;
import com.microshopper.productcatalogservice.service.ProductCatalogService;
import com.microshopper.productcatalogservice.web.dto.ProductCatalogDto;
import com.microshopper.productcatalogservice.web.dto.ProductDto;
import com.microshopper.productreviewservice.web.api.ProductReviewApi;
import com.microshopper.productreviewservice.web.dto.ProductReview;
import com.microshopper.productservice.web.api.ProductApi;
import com.microshopper.productservice.web.dto.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductCatalogServiceImpl implements ProductCatalogService {

  private final ProductApi productApi;
  private final ManufacturerApi manufacturerApi;
  private final ProductReviewApi productReviewApi;

  @Autowired
  public ProductCatalogServiceImpl(
    ProductApi productApi,
    ManufacturerApi manufacturerApi,
    ProductReviewApi productReviewApi) {

    this.productApi = productApi;
    this.manufacturerApi = manufacturerApi;
    this.productReviewApi = productReviewApi;
  }

  @Override
  public ProductDto getProduct(Long id) {

    ProductDto productDto = new ProductDto();

    Product product = productApi.getProduct(id);
    List<ProductReview> productReviews = productReviewApi.getAllProductReviews(id);
    Manufacturer manufacturer = manufacturerApi.getManufacturer(product.getManufacturerId());

    productDto.setData(product);
    productDto.setReviews(productReviews);
    productDto.setManufacturer(manufacturer);

    return productDto;
  }

  @Override
  public ProductCatalogDto getProductCatalog() {
    return null;
  }

  @Override
  public List<Product> getTopSoldProducts(int top) {
    return productApi.getAllProducts().stream().limit(top).collect(Collectors.toList());
  }

  @Override
  public List<Product> getTopReviewedProducts(int top) {
    return productApi.getAllProducts().stream().limit(top).collect(Collectors.toList());
  }

  @Override
  public List<Manufacturer> getTopManufacturers(int top) {
    return manufacturerApi.getAllManufacturers().stream().limit(top).collect(Collectors.toList());
  }
}
