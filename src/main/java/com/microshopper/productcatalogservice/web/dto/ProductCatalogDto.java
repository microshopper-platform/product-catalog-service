package com.microshopper.productcatalogservice.web.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonRootName(value = "ProductCatalog")
public class ProductCatalogDto {

  private List<ProductDto> products;
}
