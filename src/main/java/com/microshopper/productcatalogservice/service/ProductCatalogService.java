package com.microshopper.productcatalogservice.service;

import com.microshopper.manufacturerservice.web.dto.Manufacturer;
import com.microshopper.productcatalogservice.web.dto.ProductDto;
import com.microshopper.productcatalogservice.web.dto.ProductCatalogDto;
import com.microshopper.productservice.web.dto.Product;

import java.util.List;

public interface ProductCatalogService {

  ProductDto getProduct(Long id);

  ProductCatalogDto getProductCatalog();

  List<Product> getTopSoldProducts(int top);

  List<Product> getTopReviewedProducts(int top);

  List<Manufacturer> getTopManufacturers(int top);
}
