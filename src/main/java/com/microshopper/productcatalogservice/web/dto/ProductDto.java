package com.microshopper.productcatalogservice.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.microshopper.manufacturerservice.web.dto.Manufacturer;
import com.microshopper.productreviewservice.web.dto.ProductReview;
import com.microshopper.productservice.web.dto.Product;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonRootName(value = "CompleteProductInformation")
public class ProductDto {

  @JsonIgnoreProperties(value = "manufacturerId")
  private Product data;

  private Manufacturer manufacturer;

  @JsonIgnoreProperties(value = "productId")
  private List<ProductReview> reviews;
}
