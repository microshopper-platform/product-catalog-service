package com.microshopper.productcatalogservice.configuration.apiclient;

import com.microshopper.productreviewservice.web.api.ProductReviewApi;
import com.microshopper.productreviewservice.web.restclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ProductReviewApiClientConfiguration {

  private final RestTemplate restTemplate;

  @Autowired
  public ProductReviewApiClientConfiguration(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Bean
  public ApiClient productReviewApiClient() {
    ApiClient apiClient = new ApiClient(restTemplate);
    apiClient.setBasePath("http://product-review-service");
    return apiClient;
  }

  @Bean
  public ProductReviewApi productReviewApi() {
    return new ProductReviewApi(productReviewApiClient());
  }
}
