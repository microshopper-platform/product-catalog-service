package com.microshopper.productcatalogservice.configuration.apiclient;

import com.microshopper.productservice.web.api.ProductApi;
import com.microshopper.productservice.web.restclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ProductApiClientConfiguration {

  private final RestTemplate restTemplate;

  @Autowired
  public ProductApiClientConfiguration(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Bean
  public ApiClient productApiClient() {
    ApiClient apiClient = new ApiClient(restTemplate);
    apiClient.setBasePath("http://product-service");
    return apiClient;
  }

  @Bean
  public ProductApi productApi() {
    return new ProductApi(productApiClient());
  }
}
