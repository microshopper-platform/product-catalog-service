package com.microshopper.productcatalogservice.configuration.apiclient;

import com.microshopper.manufacturerservice.web.api.ManufacturerApi;
import com.microshopper.manufacturerservice.web.restclient.ApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ManufacturerApiClientConfiguration {

  private final RestTemplate restTemplate;

  @Autowired
  public ManufacturerApiClientConfiguration(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Bean
  public ApiClient manufacturerApiClient() {
    ApiClient apiClient = new ApiClient(restTemplate);
    apiClient.setBasePath("http://manufacturer-service");
    return apiClient;
  }

  @Bean
  public ManufacturerApi manufacturerApi() {
    return new ManufacturerApi(manufacturerApiClient());
  }
}
