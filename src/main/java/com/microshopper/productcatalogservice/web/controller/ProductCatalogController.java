package com.microshopper.productcatalogservice.web.controller;

import com.microshopper.manufacturerservice.web.dto.Manufacturer;
import com.microshopper.productcatalogservice.service.ProductCatalogService;
import com.microshopper.productcatalogservice.web.dto.ProductDto;
import com.microshopper.productservice.web.dto.Product;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;

@RestController
@Tag(name = "productCatalog", description = "The Product Catalog API")
public class ProductCatalogController {

  private static final String BASE_URL = "/api/product-catalog";
  private static final Integer TOP_DEFAULT = 5;

  private final ProductCatalogService productCatalogService;

  @Autowired
  public ProductCatalogController(ProductCatalogService productCatalogService) {
    this.productCatalogService = productCatalogService;
  }

  @GetMapping(value = BASE_URL + "/product/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public ProductDto getProduct(@PathVariable Long id) {

    return productCatalogService.getProduct(id);
  }

  @GetMapping(value = BASE_URL + "/products/top/sold/{top}")
  public List<Product> getTopSoldProducts(@PathVariable(required = false) Integer top) {
    return productCatalogService.getTopSoldProducts(Objects.requireNonNullElse(top, TOP_DEFAULT));
  }

  @GetMapping(value = BASE_URL + "/products/top/reviewed/{top}")
  public List<Product> getTopReviewedProducts(@PathVariable(required = false) Integer top) {
    return productCatalogService.getTopReviewedProducts(Objects.requireNonNullElse(top, TOP_DEFAULT));
  }

  @GetMapping(value = BASE_URL + "/manufacturers/top/{top}")
  public List<Manufacturer> getTopManufacturers(@PathVariable(required = false) Integer top) {
    return productCatalogService.getTopManufacturers(Objects.requireNonNullElse(top, TOP_DEFAULT));
  }
}
